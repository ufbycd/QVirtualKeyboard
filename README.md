# QVirtualKeyboard

### 介绍
Qt5虚拟键盘支持中英文,仿qt官方的virtualkeyboard模块,但使用QWidget实现。

![输入图片说明](example.png "无标题.png")

### 使用说明

#### 编译安装

```
git clone本仓库到本地
cd 进入clone下来的文件夹
mkdir build && cd build
qmake ..
make
make install
```

#### 启动时指定所使用的虚拟键盘

* 方法一，通过代码指定： 在项目main函数中设置好环境变量。代码：`qputenv("QT_IM_MODULE",QByteArray("qt5input"));`
* 方法二，通过命令行设置环境变量：`QT_IM_MODULE=qt5input <程序名>`
