#ifndef QPINYININPUTMETHOD_H
#define QPINYININPUTMETHOD_H

#include "QAbstractInputMethod.h"

class QPinyinInputMethodPrivate;
class InputContext;
class QPinyinInputMethod : public QAbstractInputMethod
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QPinyinInputMethod)
public:
    explicit QPinyinInputMethod(QObject *parent = nullptr);
    ~QPinyinInputMethod();

    InputContext *inputContext() const;
    void setInputContext(InputContext * context);
    void reset() override;
    void changeLanguage() override;
    void chooseCandidate(int id) override;
    InputMode inputMode() override;

signals:

public slots:
    bool onKeyEvent(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers) override;

private:
    QPinyinInputMethodPrivate *d_ptr;
    InputContext * m_inputContext;
};

#endif // QPINYININPUTMETHOD_H
