#include "platforminputcontextplugin.h"
#include "inputcontext.h"
#include <QDebug>

QPlatformInputContext *PlatformInputContextPlugin::create(const QString &key, const QStringList &paramList)
{
    Q_UNUSED(paramList);

    if (key.compare(key, QStringLiteral("qt5input"), Qt::CaseInsensitive) == 0)
    {

        qDebug() << "loaded qt5input keyboard";
        return new InputContext;
    }

    return NULL;
}
