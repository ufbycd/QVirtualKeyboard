#include "KeyboardContainer.h"
#include <QVBoxLayout>
#include <QtDebug>
#include <QPropertyAnimation>
#include <QApplication>
#include <QScreen>

KeyboardContainer::KeyboardContainer(QWidget *parent) : QWidget(parent)
{
	normalKeyboard = new NormalKeyboard(this);
	symbolKeyboard = new SymbolKeyboard(this);
    candidateListView = new CandidatesListWidget(this);
    candidateListView->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    candidateListView->setMinimumSize(400, 40);

    mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(candidateListView);

    QWidget * widgetContainer = new QWidget(this);
	bottomLayout = new QHBoxLayout;
	bottomLayout->addWidget(normalKeyboard);
	bottomLayout->addWidget(symbolKeyboard);
	bottomLayout->setMargin(0);
	widgetContainer->setLayout(bottomLayout);
    widgetContainer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

	mainLayout->addWidget(widgetContainer);

	mainLayout->setStretch(0, 0);
	mainLayout->setStretch(1, 1);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(10, 0, 10, 10);

	symbolKeyboard->hide();

        //将KeyboardContainer设置为模态窗口会导致下面的语句不起作用,虚拟键盘会抢输入框焦点
        //setWindowModality(Qt::WindowModal);
    setWindowFlags(windowFlags() | Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);

    setStyleSheet("*{font-size: 18pt;outline: none} \
    QWidget\
    {\
        background-color: #d5dde0;\
        background-clip: border;\
        border-image: none;\
        border: none;\
        border-radius: 5px;\
    }\
    QPushButton\
    {\
        color:#000000;\
        background-color:#ffffff;\
        border: none;\
        border-radius: 5px;\
    }\
    QPushButton:hover:pressed\
    {\
        background-color:#c0c0c0;\
    }\
    QPushButton:hover:!pressed\
    {\
        background-color:#f0f0f0;\
    }\
    QPushButton:disabled\
    {\
        background-color:#c0c0c0;\
    }");

    candidateListView->setStyleSheet("*{font-size: 18pt;outline: none} \
    QPushButton\
    {\
        color:#000000;\
        background-color:#ffffff;\
        border: none;\
        border-radius: 2px;\
    }\
    QPushButton:hover:pressed\
    {\
        background-color:#c0c0c0;\
    }\
    QPushButton:hover:!pressed\
    {\
        background-color:#f0f0f0;\
    }\
    QPushButton:disabled\
    {\
        background-color:#c0c0c0;\
    }");

    connect(normalKeyboard, &NormalKeyboard::changeLanguage, this, &KeyboardContainer::changeLanguage);
    connect(symbolKeyboard, &SymbolKeyboard::changeLanguage, this, &KeyboardContainer::changeLanguage);
    connect(normalKeyboard, &NormalKeyboard::changeSymbol, this, &KeyboardContainer::onChangeSymbol);
    connect(symbolKeyboard, &SymbolKeyboard::changeSymbol, this, &KeyboardContainer::onChangeSymbol);

	connect(normalKeyboard, &NormalKeyboard::hideKeyboard, this, &KeyboardContainer::hideKeyboard);
    connect(symbolKeyboard, &SymbolKeyboard::hideKeyboard, this, &KeyboardContainer::hideKeyboard);

    connect(normalKeyboard, &NormalKeyboard::keyPressed, this, &KeyboardContainer::keyPressed);
    connect(symbolKeyboard, &SymbolKeyboard::keyPressed, this, &KeyboardContainer::keyPressed);
    connect(candidateListView,&CandidatesListWidget::chooseText,this,&KeyboardContainer::chooseCandidate);

    animation = new QPropertyAnimation(this, "pos");
    animation->setDuration(300);
    connect(animation,&QAbstractAnimation::finished,this,&KeyboardContainer::onAnimationFinished);
    m_hiding = false;
}

KeyboardContainer::~KeyboardContainer()
{
    if(isAnimating())
        animation->stop();
}

bool KeyboardContainer::isAnimating() const
{
    return animation->state() == QAbstractAnimation::Running;
}

void KeyboardContainer::setCandidateList(const QStringList &texts)
{
    candidateListView->setCandidatesList(texts);
}

void KeyboardContainer::onInputModeChanged(QAbstractInputMethod::InputMode mode)
{
    QString inputName;

    switch(mode)
    {
    case QAbstractInputMethod::InputMode::Latin:
        inputName = "English";
        break;

    case QAbstractInputMethod::InputMode::Pinyin:
        inputName = "拼音";
        break;

    default:
        qCritical() << "unknown input mode:" << &mode;
        break;
    }

    normalKeyboard->setCurLanguage(inputName);
    symbolKeyboard->setCurLanguage(inputName);
}

void KeyboardContainer::onChangeSymbol()
{
    if(normalKeyboard->isVisible())
    {
        normalKeyboard->hide();
        symbolKeyboard->show();
    }
    else {
        symbolKeyboard->hide();
        normalKeyboard->show();
    }

    emit changeSymbol();
}

void KeyboardContainer::onHideSymbol()
{
    if(!normalKeyboard->isVisible())
    {
        symbolKeyboard->hide();
        normalKeyboard->show();
    }
}

void KeyboardContainer::animationHide()
{
    int screenHeight = qApp->primaryScreen()->size().height();

    if(isAnimating())
        animation->stop();

    m_hiding = true;

    animation->setStartValue(QPoint(pos().x(),pos().y()));
    animation->setEndValue(QPoint(pos().x(),screenHeight));
    animation->start();
}

void KeyboardContainer::animationShow()
{
    move(this->pos().x(),qApp->primaryScreen()->size().height());

    show();

    int screenHeight = qApp->primaryScreen()->size().height();

    animation->setStartValue(QPoint(pos().x(),screenHeight));
    animation->setEndValue(QPoint(pos().x(),screenHeight - height()));
    animation->start();
}

void KeyboardContainer::setInputMethodHints(Qt::InputMethodHints hints)
{
    Q_UNUSED(hints);
}

void KeyboardContainer::onAnimationFinished()
{
    if(m_hiding)
        hide();

    m_hiding = false;
}

// FIXME 此函数工作不正确
void KeyboardContainer::setCandidateVisible(bool visible)
{
    if(visible == candidateListView->isVisible())
    {
        return;
    }

    auto candidateHeight = candidateListView->height();
    auto height = this->height();
    candidateListView->setVisible(visible);
    if(visible)
    {
        height += candidateHeight;
    }
    else
    {
        height -= candidateHeight;
    }

    setFixedHeight(height);
}
