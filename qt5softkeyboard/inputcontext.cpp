#define QT_NO_DEBUG_OUTPUT

#include <QtDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QDesktopWidget>
#include <QtWidgets/QMainWindow>
#include <QInputDialog>
#include <QWindow>
#include <QKeyEvent>
#include <QDebug>
#include <QScreen>
#include <QThread>

#include "inputcontext.h"
#include "QPinyinInputMethod.h"
#include "QKeyboardLayout.h"

#define VIRTUALKEYBOARD_DEBUG qDebug

InputContext::InputContext() :
    QPlatformInputContext(),
        m_inputPanel(nullptr),
	m_focusObject(nullptr),
    m_ownEvent(nullptr)
{
    screen = qApp->primaryScreen()->geometry();

	inputMethod = new QPinyinInputMethod(this);
	qobject_cast<QPinyinInputMethod*>(inputMethod)->setInputContext(this);
	inputMethodHints = Qt::ImhNone;

	QKeyboardLayout().LoadLayout(":/layout/main.json");
    prepareInputPanel();
}

InputContext::~InputContext()
{
	if (m_inputPanel) {
		delete m_inputPanel;
		m_inputPanel = nullptr;
	}
}

bool InputContext::isValid() const
{
	return true;
}

QRectF InputContext::keyboardRect() const
{
	return m_inputPanel ? m_inputPanel->rect() : QRect();
}

bool InputContext::isAnimating() const
{
    return m_inputPanel ? m_inputPanel->isAnimating() : false;
}

void InputContext::commit(const QString &text, int replaceFrom, int replaceLength)
{
	QList<QInputMethodEvent::Attribute> attributes;
	//attributes.append(QInputMethodEvent::Attribute(QInputMethodEvent::Selection,0,0,QVariant()));

	QInputMethodEvent inputEvent(QString(), attributes);
	inputEvent.setCommitString(text, replaceFrom, replaceLength);

	sendEvent(&inputEvent);
}

void InputContext::commit()
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::commit()";
}

void InputContext::reset()
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::reset()";
    if (inputMethod)
        inputMethod->reset();
}

bool InputContext::isInputPanelVisible() const
{
    return m_inputPanel ? m_inputPanel->isVisible() : false;
}

void InputContext::showInputPanel()
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::showInputPanel()";

    if(m_inputPanel == nullptr) {
        prepareInputPanel();
    }

    if(! m_inputPanel->isVisible()) {
        m_inputPanel->show();
    }
}

void InputContext::hideInputPanel()
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::hideInputPanel()";

    if(m_inputPanel == nullptr) {
        return;
    }

    if(m_inputPanel->isVisible()) {
        m_inputPanel->hide();
    }
}

void InputContext::setFocusObject(QObject *object)
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::setFocusObject():" << object;

    if (m_focusObject != object) {
        if (m_focusObject) {
            m_focusObject->removeEventFilter(this);
            if (!preeditText.isEmpty()) {
                setPreeditText("");
            }
		}

		m_focusObject = object;
        if (m_focusObject) {
            m_focusObject->installEventFilter(this);

            QPoint point = QCursor::pos();
            VIRTUALKEYBOARD_DEBUG() << "focus point:" << point;

            QRect rect = m_inputPanel->geometry();
            if(point.y() > (screen.height()/2)) {
                rect.setY(0);
            } else {
                rect.setY(screen.y() + screen.height() - rect.height());
            }

            VIRTUALKEYBOARD_DEBUG() << "input panel:" << rect;
            m_inputPanel->setGeometry(rect);
        }

		//emit focusObjectChanged();
	}

	update(Qt::ImQueryAll);
}

QVariant InputContext::inputMethodQuery(Qt::InputMethodQuery query)
{
    QInputMethodQueryEvent event(query);
    sendEvent(&event);
    return event.value(query);
}

void InputContext::update(Qt::InputMethodQueries queries)
{
    VIRTUALKEYBOARD_DEBUG() << "InputContext::update():" << queries;

    if(m_focusObject == nullptr && isInputPanelVisible()){
        hideInputPanel();
        return;
    }

    bool imEnabled = inputMethodQuery(Qt::ImEnabled).toBool();
    if (imEnabled) {
        showInputPanel();
    } else {
        hideInputPanel();
    }

    if(isInputPanelVisible()) {
        auto imHints = inputMethodQuery(Qt::ImHints).value<Qt::InputMethodHints>();

        if(imHints != inputMethodHints) {
            inputMethodHints = imHints;
            m_inputPanel->setInputMethodHints(imHints);
        }
    }
}

void InputContext::sendKeyEvent(QKeyEvent *event)
{
    if (m_focusObject) {
        m_ownEvent = event;
        QGuiApplication::sendEvent(m_focusObject, event);
        m_ownEvent = nullptr;
    } else {
        qWarning() << "InputContext: no focus object!";
    }
}

void InputContext::sendEvent(QEvent *event)
{
    if (m_focusObject) {
        m_ownEvent = event;
		QGuiApplication::sendEvent(m_focusObject, event);
        m_ownEvent = nullptr;
    }
}

void InputContext::prepareInputPanel()
{
    m_inputPanel = new KeyboardContainer();
    m_inputPanel->setObjectName("Qt5KeyBoard");
    connect(m_inputPanel, &KeyboardContainer::hideKeyboard, this, &InputContext::hideKeyboard);
    connect(m_inputPanel, &KeyboardContainer::keyPressed, inputMethod, &QAbstractInputMethod::onKeyEvent);
    connect(m_inputPanel, &KeyboardContainer::changeLanguage, inputMethod, &QAbstractInputMethod::changeLanguage);
    connect(m_inputPanel, &KeyboardContainer::chooseCandidate, inputMethod, &QAbstractInputMethod::chooseCandidate);

    connect(inputMethod, &QAbstractInputMethod::showCandidateList, m_inputPanel, &KeyboardContainer::setCandidateList);
    connect(inputMethod, &QAbstractInputMethod::inputModeChanged, m_inputPanel, &KeyboardContainer::onInputModeChanged);

    connect(m_inputPanel, &QObject::destroyed, this, [this]() {m_inputPanel = nullptr;});

    int w = 1000;
    int h = 340;
    int x = screen.x() + (screen.width() - w) / 2;
    int y = screen.y() + screen.height() - h;
    const QRect rect(x, y, w, h);
    m_inputPanel->setGeometry(rect);
    m_inputPanel->setMaximumSize(w, h);
    m_inputPanel->hide();
}

void InputContext::hideKeyboard()
{
	if (m_inputPanel)
	{
		m_inputPanel->hide();
        m_inputPanel->onHideSymbol();
    }
}

bool InputContext::eventFilter(QObject *obj, QEvent *event)
{
    Q_UNUSED(obj);
    bool ret = false;

    if(event == m_ownEvent || ! isInputPanelVisible()) {
        return false;
    }

    qDebug() << "eventfilter:" << obj << event;
    if(event->type() == QEvent::KeyPress) {
        auto keyEvent = dynamic_cast<QKeyEvent *>(event);

        if(keyEvent == nullptr) {
            return false;
        }

        ret = true;
        auto key = static_cast<Qt::Key>(keyEvent->key());
        if(key == Qt::Key_Escape) {
            hideInputPanel();
        } else if(keyEvent->modifiers() == Qt::ControlModifier && key == Qt::Key_Space) {
            if(! m_inputPanel->isVisible()) {
                showInputPanel();
            } else {
                inputMethod->changeLanguage();
            }
        } else {
            inputMethod->onKeyEvent(key, keyEvent->text(), keyEvent->modifiers());
        }
    }

    return ret;
}

bool testAttribute(const QList<QInputMethodEvent::Attribute> &attributes, QInputMethodEvent::AttributeType attributeType)
{
	for (const QInputMethodEvent::Attribute &attribute : qAsConst(attributes)) {
		if (attribute.type == attributeType)
			return true;
	}
	return false;
}

void InputContext::sendPreedit(const QString &text, const QList<QInputMethodEvent::Attribute> &attributes, int replaceFrom, int replaceLength)
{
	preeditText = text;

	QInputMethodEvent event(text, attributes);
	const bool replace = replaceFrom != 0 || replaceLength > 0;
	if (replace)
		event.setCommitString(QString(), replaceFrom, replaceLength);

	sendEvent(&event);

	// Send also to shadow input if only attributes changed.
	// In this case the update() may not be called, so the shadow
	// input may be out of sync.
	//    if (_shadow.inputItem() && !replace && !text.isEmpty() &&
	//            !textChanged && attributesChanged) {
	//        VIRTUALKEYBOARD_DEBUG() << "QVirtualKeyboardInputContextPrivate::sendPreedit(shadow)"
	//#ifdef SENSITIVE_DEBUG
	//               << text << replaceFrom << replaceLength
	//#endif
	//            ;
	//        event.setAccepted(true);
	//        QGuiApplication::sendEvent(_shadow.inputItem(), &event);
	//}
}

void InputContext::setPreeditText(const QString &text, QList<QInputMethodEvent::Attribute> attributes, int replaceFrom, int replaceLength)
{
	// Add default attributes
	if (!text.isEmpty()) {
		if (!testAttribute(attributes, QInputMethodEvent::TextFormat)) {
			QTextCharFormat textFormat;
			textFormat.setUnderlineStyle(QTextCharFormat::SingleUnderline);
			attributes.append(QInputMethodEvent::Attribute(QInputMethodEvent::TextFormat, 0, text.length(), textFormat));
		}
	} /*else if (d->_forceCursorPosition != -1) {
		d->addSelectionAttribute(attributes);
	}*/

	sendPreedit(text, attributes, replaceFrom, replaceLength);
}

void InputContext::sendKeyClick(int key, const QString &text, int modifiers)
{
	QKeyEvent pressEvent(QEvent::KeyPress, key, Qt::KeyboardModifiers(modifiers), text);
	QKeyEvent releaseEvent(QEvent::KeyRelease, key, Qt::KeyboardModifiers(modifiers), text);

	sendKeyEvent(&pressEvent);
	sendKeyEvent(&releaseEvent);
}
