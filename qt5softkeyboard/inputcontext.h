#ifndef INPUTCONTEXT_H
#define INPUTCONTEXT_H

#include <qpa/qplatforminputcontext.h>
#include <QInputMethodEvent>
#include <QRect>

#include "KeyboardContainer.h"
#include "QKeyboardLayout.h"

class InputContextPrivate;
class QAbstractInputMethod;
class InputContext : public QPlatformInputContext
{
    Q_OBJECT
public:
    explicit InputContext();
    ~InputContext();

    bool isValid() const override;
    QRectF keyboardRect() const override;
    bool isInputPanelVisible() const override;
    void update(Qt::InputMethodQueries queries) override;
    void setFocusObject(QObject *object) override;
    void hideInputPanel() override;
    void showInputPanel() override;
    bool isAnimating() const override;
    Q_INVOKABLE void commit() override;
    void reset() override;

    Q_INVOKABLE void commit(const QString &text, int replaceFrom = 0, int replaceLength = 0);
    void sendKeyClick(int key, const QString &text, int modifiers = 0);
    void sendKeyEvent(QKeyEvent *event);
    void setPreeditText(const QString &text,QList<QInputMethodEvent::Attribute> attributes = QList<QInputMethodEvent::Attribute>(),int replaceFrom = 0, int replaceLength = 0);
    void sendEvent(QEvent *event);

signals:    
	void inputMethodHintsChanged();

private:
    QRect screen;
    KeyboardContainer * m_inputPanel;
    QObject *m_focusObject;
    QEvent *m_ownEvent;
    QAbstractInputMethod * inputMethod;
    QString preeditText;
    QKeyboardLayout mainLayout;
    QKeyboardLayout symbolLayout;
    Qt::InputMethodHints inputMethodHints = Qt::ImhNone;

    void prepareInputPanel();

private slots:
    void hideKeyboard();

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;
    void sendPreedit(const QString &text, const QList<QInputMethodEvent::Attribute> &attributes, int replaceFrom, int replaceLength);
    QVariant inputMethodQuery(Qt::InputMethodQuery query);
};

#endif // INPUTCONTEXT_H
