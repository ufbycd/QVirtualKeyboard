#!/usr/bin/python

import sys
import re

def lastLogInfo(changelogPath):
    version = ""
    rev = "1"
    with open(changelogPath) as changelog:
        line = ""
        while len(line) == 0 or line.isspace():
            line = changelog.readline()
        m = re.search("\((.+)\)", line)
        version = m.groups()[0]

    syms = version.split('-')
    version = syms[0]
    if len(syms) > 1:
        rev = syms[1]

    return version, rev

if __name__ == "__main__":
    if len(sys.argv) < 2:
        changelogPath = "debian/changelog"
    else:
        changelogPath = sys.argv[1]
    version, _= lastLogInfo(changelogPath)
    print(version)

